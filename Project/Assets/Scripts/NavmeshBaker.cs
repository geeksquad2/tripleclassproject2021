﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavmeshBaker : MonoBehaviour
{
    //Dynamically bakes navmesh based on meshes affected by object movement

    [Header("NavMeshSurface Menu")]
    public NavMeshSurface[] surfaces;
    [SerializeField]
    [Range(0, 10)]
    private int currentMeshNumber;

    // Update is called once per frame
    void Update()
    {
        surfaces[currentMeshNumber].BuildNavMesh();
    }
}
