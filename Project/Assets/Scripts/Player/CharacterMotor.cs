﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour
{
    Animator animatorComponent;


    private void Awake()
    {
        animatorComponent = GetComponentInChildren<Animator>();
    }
    /// <summary>
    /// Called on by controllers to move an object, given the "x and y", speed, and rigidbody parameters
    /// </summary>
    /// <param name="xInput">Amount of movement in x</param>
    /// <param name="yInput">Amount of movement in z (y on 2D ground plane)</param>
    /// <param name="speed">Multiplier of speed to apply</param>
    /// <param name="rigidbodyComponent">Rigidbody attached to the object you wish to move</param>
    public void MoveChar(float xInput, float yInput, float speed, Rigidbody rigidbodyComponent)
    {
        if (enabled)
        {
            if (Mathf.Abs(xInput) > 0.2f || Mathf.Abs(yInput) > 0.2f)
            {
                animatorComponent.SetBool("Running", true);
            }
            else
            {
                animatorComponent.SetBool("Running", false);
            }
            rigidbodyComponent.velocity = new Vector3(xInput * speed,
                                                  rigidbodyComponent.velocity.y,
                                                  yInput * speed);
        }
    }
}
