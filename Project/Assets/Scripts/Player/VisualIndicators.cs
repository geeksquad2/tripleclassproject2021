﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualIndicators : MonoBehaviour
{
    //This script controls the indicators that tell the player when magnets are being used

    public GameObject disabledVisual1;
    public GameObject disabledVisual2;

    void Start()
    {
        disabledVisual1.SetActive(false);
        disabledVisual2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // The indicator shows as long as you hold down the button
        // It disappears when you let go

        if (Input.GetButtonDown("Fire1"))
        {
            disabledVisual1.SetActive(true);

        }

        if (Input.GetButtonDown("Fire2"))
        {
            disabledVisual2.SetActive(true);
        }


        if (Input.GetButtonUp("Fire1"))
        {
            disabledVisual1.SetActive(false);
        }

        if (Input.GetButtonUp("Fire2"))
        {
            disabledVisual2.SetActive(false);
        }
    }
}
