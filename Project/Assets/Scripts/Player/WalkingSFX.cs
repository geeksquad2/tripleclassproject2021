﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingSFX : MonoBehaviour
{
    public AudioSource footstep;

    // Start is called before the first frame update
    void Start()
    {
        footstep = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
        {
            footstep.Play();
        }

        //when let go, the music stops

        if (Input.GetButtonUp("Horizontal") || Input.GetButtonUp("Vertical"))
        {
            footstep.Stop();
        }

    }
}
