﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //This script controlls the player movement and actions 

    Vector3 lastGroundedPosition;

    [SerializeField]
    float m_speed;

    [SerializeField]
    float m_magnetDistance;

    [SerializeField]
    float m_magnetForce;

    public LayerMask targetLayer;

    //Minimum distance allowed for magnet attraction
    [SerializeField]
    float m_minimumDistance;

    Rigidbody rigidbodyComponent;
    CharacterMotor characterMotor;

    GameObject currentObject;

    float xInput, yInput;

    Vector3 direction, lastDirection;
    Vector3 startPosition;
    float groundHeight;

    bool freezeRotation;
    Vector3 rayPosition;

    //Audio

    AudioSource audioSource;
    public AudioClip mode1;
    public AudioClip mode2;
    public float volume = 0.5f;
    public AudioSource magnetUse;

    Animator animatorComponent;

    private void Awake()
    {
        rigidbodyComponent = GetComponent<Rigidbody>();
        characterMotor = GetComponent<CharacterMotor>();
        animatorComponent = GetComponentInChildren<Animator>();
        startPosition = transform.position;

        //Ground height is defined by default for the sake of Land() until you touch the actual ground
        groundHeight = 0.5f;
        lastGroundedPosition = transform.position; 
    }

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        //Application of movement calculations in Update
        characterMotor.MoveChar(xInput, yInput, m_speed, rigidbodyComponent);
    }

    // Update is called once per frame
    void Update()
    {
        //Calculate the input on the given frame, used for the next FixedUpdate call
        xInput = Input.GetAxisRaw("Horizontal");
        yInput = Input.GetAxisRaw("Vertical");

        if (!freezeRotation)
            CalculateDirection();

        GroundCheck(); 

        #region Magnet sounds
        // SFX connected to magnet usage
        if (Input.GetButtonDown("Fire1"))
        {
            audioSource.PlayOneShot(mode2, volume);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            audioSource.PlayOneShot(mode1, volume);
        }

        if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2"))
        {
            magnetUse.Play();
        }

        if (Input.GetButtonUp("Fire1") || Input.GetButtonUp("Fire2"))
        {
            audioSource.Stop();
        }

        #endregion

        #region Magnet logic

        //Calculating magnet logic
        if (Input.GetButton("Fire1") || Input.GetButton("Fire2"))
        {


            //Inverse polarity to "push" instead of pull
            int polarity = 1;
            animatorComponent.SetBool("Pull", true);
            if (Input.GetButton("Fire2"))
            {
                animatorComponent.SetBool("Push", true);
                animatorComponent.SetBool("Pull", false);
                polarity = -1;
            }

            //Check for objects to attract
            RaycastHit boxHit, rayHit;
            rayPosition = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);

            /*
             * Box cast fires to check for entities to use the magnet with.  Fires a supplementary cast to 
             * peel around corners if necessary.  The casts are looking for walls and Magnetable Objects
             */

            if (Physics.BoxCast(rayPosition, new Vector3(1f, 0.2f, 1f), transform.forward, out boxHit, Quaternion.identity, m_magnetDistance))
            {
                //If you didn't hit an enemy or object (assuming a wall), check if you're on a corner
                if (boxHit.collider.gameObject.layer != 8 &&
                boxHit.collider.gameObject.layer != 11)
                {
                    if (Physics.Raycast(rayPosition, transform.forward, out rayHit, m_magnetDistance))
                    {
                        //If you're still presumably hitting a wall, let go of any object you're holding
                        if (rayHit.collider.gameObject.layer != 8 &&
                            rayHit.collider.gameObject.layer != 11)
                        {
                            if (currentObject != null)
                            {
                                if (currentObject.tag == "Movable" || currentObject.tag == "Enemy")
                                    currentObject.SendMessage("LetGo");
                            }
                            currentObject = null;
                            freezeRotation = false;
                            return;
                        }

                        //Your ray hit something your boxcast did not: assumed "around-corner" edge case
                        else
                        {
                            boxHit = rayHit;
                        }
                    }
                }

                //If your currently hit object is different from the last object, let go of the last object
                if (currentObject != null && boxHit.collider.gameObject != currentObject)
                {
                    if (currentObject.tag == "Movable" || currentObject.tag == "Enemy")
                        currentObject.SendMessage("LetGo");
                }

                //Reduce sliding
                rigidbodyComponent.velocity = Vector3.zero;

                //Assign the current object with whatever you hit (ray or box)
                currentObject = boxHit.collider.gameObject;

                //Let go if an object is too close to pull
                if (boxHit.distance < m_minimumDistance && Input.GetButton("Fire1"))
                {
                    if (boxHit.collider.tag == "Movable")
                    {
                        currentObject.GetComponent<MoveFromMagnet>().LetGo();
                    }

                    else if (boxHit.collider.tag == "Enemy")
                    {
                        currentObject.GetComponent<EnemyBehaviourManager>().GetBacktoNormal();
                        currentObject.GetComponent<EnemyHit>().LetGo();

                    }
                    return;
                }


                //If stationary, push/pull yourself
                if (boxHit.collider.tag == "Stationary")
                {
                    Fly(transform.forward * polarity);
                    freezeRotation = true;
                }

                //else push/pull object towards you
                else if (boxHit.collider.tag == "Movable")
                {
                    currentObject.GetComponent<MoveFromMagnet>().AddMagneticForce(transform.forward * -1 * polarity);
                    characterMotor.enabled = false;
                    animatorComponent.SetBool("Running", false);
                    freezeRotation = true;
                }

                //Push/pull an enemy
                else if (boxHit.collider.tag == "Enemy")
                {
                    currentObject.GetComponent<EnemyBehaviourManager>().GetHit();
                    currentObject.GetComponent<EnemyHit>().AddMagneticForce(transform.forward * -1 * polarity);
                    characterMotor.enabled = false;
                    animatorComponent.SetBool("Running", false);
                    freezeRotation = true;
                }

                //Push/pull a mine cart
                else if (boxHit.collider.tag == "Kart")
                {
                    currentObject.GetComponent<MineCart>().AddMagneticForce(transform.forward * -1 * polarity);
                    characterMotor.enabled = false;
                    animatorComponent.SetBool("Running", false);
                    freezeRotation = true;
                }
            }
        }

        //On letting go of the mouse buttons, let go of the appropriate object
        if (Input.GetButtonUp("Fire1") || Input.GetButtonUp("Fire2"))
        {
            animatorComponent.SetBool("Pull", false);
            animatorComponent.SetBool("Push", false);
            if (currentObject != null && currentObject.tag == "Movable")
            {
                currentObject.GetComponent<MoveFromMagnet>().LetGo();
                freezeRotation = false;
            }

            if (currentObject != null && currentObject.tag == "Kart")
            {
                currentObject.GetComponent<MineCart>().LetGo();
                freezeRotation = false;
            }

            if (currentObject != null && currentObject.tag == "Enemy")
            {
                currentObject.GetComponent<EnemyBehaviourManager>().GetBacktoNormal();
                currentObject.GetComponent<EnemyHit>().LetGo();
                freezeRotation = false;
            }

            else if (currentObject != null && currentObject.tag == "Stationary")
            {
                freezeRotation = false;
                Land();
            }

            characterMotor.enabled = true;
            rigidbodyComponent.useGravity = true;
        }
        #endregion
    }

    void GroundCheck()
    {
        RaycastHit hitInfo = new RaycastHit();

        if (Physics.Raycast(transform.position + transform.up, transform.up * -1, out hitInfo, 1.3f))
        {
            if(hitInfo.transform.gameObject.layer == 0 || hitInfo.transform.gameObject.layer == 14)
            {
                
               lastGroundedPosition = transform.position;
            }
        }
    }

    //Float the character off the ground in desired direction
    void Fly(Vector3 direction)
    {
        animatorComponent.SetBool("Running", false);
        direction = direction.normalized;

        characterMotor.enabled = false; 
        rigidbodyComponent.useGravity = false;
        rigidbodyComponent.position = new Vector3(rigidbodyComponent.position.x, groundHeight, rigidbodyComponent.position.z);

        rigidbodyComponent.velocity = direction * m_magnetForce; 
    }

    //Reset the player state when they land on the ground
    void Land()
    {
        characterMotor.enabled = true;
        rigidbodyComponent.useGravity = true;
        rigidbodyComponent.position = new Vector3(rigidbodyComponent.position.x, groundHeight, rigidbodyComponent.position.z);
    }
    
    //Calculate the direction for the magnet ability
    void CalculateDirection()
    {
        direction = new Vector3(transform.position.x + xInput, transform.position.y, transform.position.z + yInput)
                                - transform.position;

        Vector3 newRotation = Vector3.RotateTowards(transform.forward, direction, 500, 0);

        transform.rotation = Quaternion.LookRotation(newRotation);

        transform.rotation = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y, 0));
    }

    //Reset position on death
    public void ResetPosition()
    {
        transform.position = lastGroundedPosition;
    }

    
    private void OnCollisionEnter(Collision collision)
    {
        //If on the ground, set a new ground height for magnet pull
        if (collision.gameObject.layer == 14)
        {
            groundHeight = collision.transform.position.y + 0.5f;
        }
    }
}
