﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class PlayerHealth : MonoBehaviour
{
    //This script controls the player's health and damage

    [SerializeField]
    GameObject skeleton; 

    Animator animatorComponent;
    public float timer;

    public int hp;
    PlayerController controller;
    public List<Material> mats = new List<Material>(); 

    bool invincible;

    AudioSource audioSource;

    public AudioClip magnusHurt;
    public AudioClip magnusDeath;


    [SerializeField]
    GameObject uiHealthBar;

    public List<GameObject> uiHearts = new List<GameObject>();

    private Dictionary<string, object> parameters = new Dictionary<string, object>();

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        invincible = false; 
        controller = GetComponent<PlayerController>();
        animatorComponent = GetComponentInChildren<Animator>();

        uiHearts[0] = uiHealthBar.transform.GetChild(0).gameObject;
        uiHearts[1] = uiHealthBar.transform.GetChild(1).gameObject;
        uiHearts[2] = uiHealthBar.transform.GetChild(2).gameObject;

        parameters.Add("deaths_1", 0);
        parameters.Add("time_1", 0);

        timer = Time.time;
    }

    private void OnDisable()
    {
        for (int i = 0; i < mats.Count; i++)
        {
            mats[i].color = Color.white;
        }
    }

    //Take specified amount of damage and print statement when hp is zero 
    public void TakeDamage(int amount)
    {
        if (!invincible)
        {
            animatorComponent.SetTrigger("Hurt");
            hp -= amount;
            invincible = true; 
            uiHearts[hp].SetActive(false);
            StartCoroutine(ColourShift());

            if (hp <= 0)
            {
                Die();
            }
        }
    }

    void Die()
    {
        timer = Time.time - timer;
        int scene = SceneManager.GetActiveScene().buildIndex;
        int deaths = PlayerPrefs.GetInt("deaths_" + scene, 0);
        PlayerPrefs.SetInt("deaths_" + scene, deaths + 1);

        StartCoroutine(Death());
    }

    IEnumerator Death()
    {
        audioSource.PlayOneShot(magnusDeath, 2.0f);
        transform.GetChild(2).gameObject.SetActive(false);
        Instantiate(skeleton, transform.position, Quaternion.identity);

        yield return new WaitForSeconds(3);

        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene);
    }

    //Flash red on damage
    IEnumerator ColourShift()
    {
        for (int i = 0; i < mats.Count; i++)
        {
            mats[i].color = Color.red; 
        }
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < mats.Count; i++)
        {
            mats[i].color = Color.white;
        }
        invincible = false; 
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Reset position and take damage upon collision with lava
        if (collision.gameObject.tag == "Lava")
        {
            TakeDamage(1);
            audioSource.PlayOneShot(magnusHurt, 2.0f);
            controller.ResetPosition();
        }
        //Take damage upon collision with enemies
        else if (collision.gameObject.tag == "Enemy")
        {
            TakeDamage(1);
            audioSource.PlayOneShot(magnusHurt, 2.0f);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        //Take damage on entering a fire trigger
        if(other.gameObject.tag == "Fire")
        {
            TakeDamage(1);
        }
    }
}
