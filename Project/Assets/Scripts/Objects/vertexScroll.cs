﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vertexScroll : MonoBehaviour
{
    [SerializeField]
    float scrollX = 0.5f;

    [SerializeField]
    float scrollY = 0.5f;

    [SerializeField]
    float wavelength = 0.1f;

    float t;

    Renderer texture;
    private void Start()
    {
        texture = GetComponentInChildren<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        t += Time.deltaTime;
        texture.material.mainTextureOffset = new Vector2(scrollX, scrollY) * Time.time + new Vector2(-1, 1) * Mathf.Sin(t) * wavelength;

        if (t >= 2 * Mathf.PI)
        {
            t = 0;
        }
    }
}
