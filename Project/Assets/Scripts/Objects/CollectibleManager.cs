﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CollectibleManager : Singleton<CollectibleManager>
{
    bool gymUnlocked, artUnlocked;

    [SerializeField]
    GameObject gymButton, artButton;

    Animator collectiblePanel;
    Text collectibleText;

    public int totalCount;
    public int currentCount;

    // Start is called before the first frame update
    void Start()
    {
        //If you're on the start menu, check if the unlockables should be enabled
        if (SceneManager.GetActiveScene().name == "Start")
        {
            MenuUIUpdate();
            totalCount = PlayerPrefs.GetInt("Count", 0);
        }

        //If you're in a regular game scene, ready the collectible counters for tallying in th level
        else
        {
            currentCount = 0;
            //Check for any collectibles that should not be there and disable them
            CollectibleDisableCheck();
            
            //Ready the counters and components needed for the collect event
            Collectible.gotCollectible.AddListener(CollectibleUpdate);
            totalCount = PlayerPrefs.GetInt("Count", 0);
            collectiblePanel = transform.GetChild(0).GetComponent<Animator>();
            collectibleText = collectiblePanel.transform.GetChild(1).GetComponent<Text>();
            collectibleText.text = totalCount.ToString();
        }
    }

    private void Update()
    {
        //adds 1 collectible to test unlocks/functionality (DEBUG!)
        if (Input.GetKeyDown(KeyCode.U))
        {
            CollectibleUpdate(null);
        }
    }

    //Check for any collectibles that should not be there and disable them
    void CollectibleDisableCheck()
    {
        Collectible[] collectibles = GameObject.FindObjectsOfType<Collectible>();
        foreach (Collectible collectible in collectibles)
        {
            if (PlayerPrefs.GetInt(collectible.name) > 0)
            {
                collectible.gameObject.SetActive(false);
            }
        }
    }

    //Check collectible counts and set the menu options appropriately
    void MenuUIUpdate()
    {
        EventTrigger[] eventTriggers = GameObject.FindObjectsOfType<EventTrigger>();

        //If enough collectibles are collected, unlock the gym option
        if (PlayerPrefs.GetInt("GymUnlocked", 0) == 1)
        {
            gymButton.GetComponent<Button>().interactable = true;
            eventTriggers[0].enabled = false;
            //gymUnlocked = true;
        }

        //Otherwise, make sure it is disabled
        else
        {
            gymButton.GetComponent<Button>().interactable = false;
        }

        //If enough collectibles are collected, unlock the art option
        if (PlayerPrefs.GetInt("ArtUnlocked", 0) == 1)
        {
            artButton.GetComponent<Button>().interactable = true;
            eventTriggers[1].enabled = false;
            //artUnlocked = true;
        }

        //Otherwise, make sure it is disabled
        else
        {
            artButton.GetComponent<Button>().interactable = false;
        }
    }

    //Add one to the saved counter and updated the UI and set unlock flags
    public void CollectibleUpdate(GameObject collectible)
    {
        totalCount++;
        currentCount++;
        collectibleText.text = totalCount.ToString();

        //UI animation plays
        if (collectiblePanel.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            collectiblePanel.SetTrigger("PickedUp");
        }
        
        //Save count
        PlayerPrefs.SetInt("Count", totalCount);

        //Save specific collectible to the disable list
        if (collectible != null) PlayerPrefs.SetInt(collectible.name, 1);

        //Save gym unlock
        if (totalCount == 25)
        {
            PlayerPrefs.SetInt("GymUnlocked", 1);
        }

        //Save art unlock
        if (totalCount == 50)
        {
            PlayerPrefs.SetInt("ArtUnlocked", 1);
        }
    }
}
