﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CollectibleEvent : UnityEvent<GameObject> { }

public class Collectible : MonoBehaviour
{
    public static CollectibleEvent gotCollectible = new CollectibleEvent();

    GameObject deathParticles;

    private void Start()
    {
        deathParticles = transform.GetChild(0).gameObject;
        ParticleSystem.MainModule particles = deathParticles.GetComponent<ParticleSystem>().main;
        particles.startColor = GetComponent<Renderer>().material.color;
    }

    private void Update()
    {
        //Constantly be rotating
        transform.Rotate(new Vector3(0, 30 * Time.deltaTime, 0));
    }

    private void OnTriggerEnter(Collider collision)
    {
        //If you touched a player, start the Collect event logic
        if (collision.GetComponent<PlayerController>())
        {
            Collect();
        }
    }


    //Fire the event that will update the collectible manager data and destroy one's self (with Fx)
    void Collect()
    {
        gotCollectible.Invoke(gameObject);
        if (deathParticles != null)
        {
            deathParticles.SetActive(true);
            deathParticles.transform.parent = null;
        }            
        gameObject.SetActive(false);
    }
}

