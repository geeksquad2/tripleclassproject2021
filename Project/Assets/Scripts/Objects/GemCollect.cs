﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemCollect: MonoBehaviour
{
    public AudioSource gemSound;

    void Start()
    {
        gemSound = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        gemSound.Play();

    }
}
