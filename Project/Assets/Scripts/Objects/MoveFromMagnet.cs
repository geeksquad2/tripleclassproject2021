﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFromMagnet : MonoBehaviour
{
    //This script controlls objects being moved by the magnet

    public bool moving; 

    //Determined if the object will respawn upon touching lava
    [SerializeField]
    bool respawnable;

    //Speed cap determines the maximum speed of the object
    [SerializeField]
    float speedCap;

    //Magnetic force determined how fast this object will move when magnetized. Higher values = faster movement. 
    [SerializeField]
    float magnetForce;

    Vector3 startPosition;

    float groundHeight;

    Rigidbody body; 
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position; 
        body = GetComponent<Rigidbody>();
        groundHeight = 0.5f;
        moving = false; 
    }

    private void FixedUpdate()
    {
        RegulateVelocity(); 
    }

    //Add directional force to the object and turn off gravity
    public void AddMagneticForce(Vector3 direction)
    {
        moving = true; 
        body.constraints = RigidbodyConstraints.FreezeRotation;
        direction = direction.normalized;

        body.position = new Vector3(body.position.x, groundHeight, body.position.z);
        body.useGravity = false;

        body.velocity = direction * magnetForce;
    }

    //Release the object from the magnet and turn gravity back on
    public void LetGo()
    {
        moving = false; 
        body.useGravity = true;
        body.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    }

    //Cap the velocity of the object
    void RegulateVelocity()
    {
        if (body.velocity.magnitude > speedCap)
        {
            body.velocity = body.velocity.normalized * speedCap;
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        //On collision with lava respawn if respawnable
        if (collision.gameObject.tag == "Lava" && respawnable)
        {
            transform.position = startPosition; 
        } else if (collision.gameObject.tag == "Movable" || collision.gameObject.tag == "Kart")
        {
            body.constraints = RigidbodyConstraints.FreezeRotation;
        }

        if (collision.gameObject.layer == 14)
        {
            groundHeight = collision.transform.position.y + 0.5f;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Movable" || collision.gameObject.tag == "Kart")
        {
            body.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }
    }
}
