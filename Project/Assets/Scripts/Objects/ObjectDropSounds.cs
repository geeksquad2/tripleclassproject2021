﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDropSounds : MonoBehaviour
{
    public AudioSource crateSound;
    public int collisionCount = 0;

    void Start()
    {
        crateSound = GetComponent<AudioSource>();
    }

    void OnCollisionEnter (Collision collision)
    {
        //The collision count increases everytime 
        //the object collides with anything

        collisionCount++;

        //the SFX doesn't play until the object
        //collides with anything above the value
        //No more "Thuds" at the start of each scene

        if (collisionCount <= 5)
        {
            crateSound.Stop();
        }

        if (collisionCount >= 5)
        {
            crateSound.Play();
            crateSound.pitch = (Random.Range(0.5f, 1.2f));
        }

    }
}
