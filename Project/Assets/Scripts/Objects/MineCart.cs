﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineCart : MonoBehaviour
{
    //This script controls the minecart objects

    [SerializeField]
    float magneticForce;

    Rigidbody body;
    Quaternion startRotation;
    Quaternion tiltRotationR;
    Quaternion tiltRotationL;

    [SerializeField]
    bool LR;

    public bool moving; 

    Vector3 cartDirection; 

    // Start is called before the first frame update
    void Start()
    {
        moving = false;
        startRotation = transform.rotation; 
        body = GetComponent<Rigidbody>();

        Vector3 rotation = startRotation.eulerAngles;

        if (LR)
        {
            rotation += Vector3.right * 20;
            tiltRotationL = Quaternion.Euler(rotation);

            rotation -= Vector3.right * 40;
            tiltRotationR = Quaternion.Euler(rotation);
        }
        else
        {
            rotation += Vector3.forward * 20;
            tiltRotationL = Quaternion.Euler(rotation);

            rotation -= Vector3.forward * 40;
            tiltRotationR = Quaternion.Euler(rotation);
        }
    }

    //Add directional force to the object and turn off gravity
    public void AddMagneticForce(Vector3 direction)
    {
        moving = true; 
        body.constraints = RigidbodyConstraints.FreezeRotation;

        if (LR)
        {
            float angle = Vector3.Angle(direction, transform.right);
            
            if (angle <= 25)
            {
                transform.position += magneticForce * Time.deltaTime * transform.right;
            }
            else if (angle >= 155)
            {
                transform.position += magneticForce * Time.deltaTime * -transform.right;
            }
            else if (angle > 25 && angle < 70)
            {
                transform.position += magneticForce / 2 * Time.deltaTime * transform.right;
            }
            else if (angle > 110 && angle < 155)
            {
                transform.position += magneticForce / 2 * Time.deltaTime * -transform.right;
            }
            else
            {
               
                if(direction.x <= 0 && direction.z > 0)
                {
                    transform.rotation = tiltRotationL;
                } else if(direction.x <= 0 && direction.z < 0)
                {
                    transform.rotation = tiltRotationR;
                } else if(direction.x > 0 && direction.z < 0)
                {
                    transform.rotation = tiltRotationR;
                } else if(direction.x > 0 && direction.z > 0)
                {
                    transform.rotation = tiltRotationL;
                }

            }
        }
        else
        {
            float angle = Vector3.Angle(direction, transform.forward);
            if (angle <= 25)
            {
                transform.position += magneticForce * Time.deltaTime * transform.forward;
            }
            else if (angle >= 155)
            {
                transform.position += magneticForce * Time.deltaTime * -transform.forward;
            }
            else if (angle > 25 && angle < 70)
            {
                transform.position += magneticForce / 2 * Time.deltaTime * transform.forward;
            }
            else if (angle > 110 && angle < 155)
            {
                transform.position += magneticForce / 2 * Time.deltaTime * -transform.forward;
            }
            else
            {
                if (direction.x >= 0 && direction.z > 0)
                {
                    transform.rotation = tiltRotationR;
                }
                else if (direction.x < 0 && direction.z < 0)
                {
                    transform.rotation = tiltRotationL;
                }
                else if (direction.x > 0 && direction.z < 0)
                {
                    transform.rotation = tiltRotationR;
                }
                else if (direction.x < 0 && direction.z > 0)
                {
                    transform.rotation = tiltRotationL;
                }
            }
        }
    }

    public void LetGo()
    {
        moving = false; 
        transform.rotation = startRotation;
        body.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    }

}
