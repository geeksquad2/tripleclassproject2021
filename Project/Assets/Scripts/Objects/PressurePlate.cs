﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    //This object detects if something is on it and moves a desired object accordingly

    [SerializeField]
    Transform objectToMove;

    Vector3 objectStartPosition;
    [SerializeField]
    Vector3 objectEndPosition;

    [SerializeField]
    float speed;

    [SerializeField]
    List<int> layersToDetect = new List<int>(); 

    List<GameObject> weightsInsideRange = new List<GameObject>(); 

    // Start is called before the first frame update
    void Start()
    {
        objectStartPosition = objectToMove.position; 
    }

    //Detects when an object is placed on the pressure plate
    private void OnTriggerEnter(Collider other)
    {
        if (layersToDetect.Contains(other.gameObject.layer) && !weightsInsideRange.Contains(other.gameObject))
        {
            weightsInsideRange.Add(other.gameObject); 
        }
        if(weightsInsideRange.Count == 1)
        {
            StopAllCoroutines();
            StartCoroutine(Lerp(objectEndPosition));
        }
    }

    //Detects when an object is removed from the pressure plate
    private void OnTriggerExit(Collider other)
    {
        if (layersToDetect.Contains(other.gameObject.layer) && weightsInsideRange.Contains(other.gameObject))
        {
            weightsInsideRange.Remove(other.gameObject);
        }

        if(weightsInsideRange.Count <= 0)
        {
            StopAllCoroutines();
            StartCoroutine(Lerp(objectStartPosition));
        }
    }

    //Moves the desired object in response to the pressure plate's current state. 
    IEnumerator Lerp(Vector3 target)
    {
        while(objectToMove.position != target)
        {
            objectToMove.position = Vector3.MoveTowards(objectToMove.position, target, speed * Time.deltaTime);
            yield return null; 
        }
    }
}
