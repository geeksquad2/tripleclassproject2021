﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfBossTrigger : MonoBehaviour
{
    [SerializeField]
    CutsceneManager manager; 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            manager.EndLevel(); 
        }
    }
}
