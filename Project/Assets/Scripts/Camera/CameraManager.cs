﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour
{
    //This script controls the in-game camera movement

    //Camera variables for speed and if the camera can move
    [SerializeField]
    float scrollSpeed;

    [SerializeField]
    Transform player; 

    //Offset distance to follow the player at
    [SerializeField]
    Vector3 offset; 

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveCameraToPosition(); 
    }

    //Move camera to desired offset from player position
    public void MoveCameraToPosition()
    {
        Vector3 target = player.position + offset;
        transform.position = Vector3.MoveTowards(transform.position, target, scrollSpeed * Time.deltaTime);
    }

}


