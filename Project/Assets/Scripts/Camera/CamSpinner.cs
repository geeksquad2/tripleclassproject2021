﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSpinner : MonoBehaviour
{
    //This script controls the camera movement in the art demo

        public Vector3 RotationPerSecond;

        void Update()
        {
            transform.Rotate(RotationPerSecond * Time.deltaTime);
        }
    
}
