﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    [SerializeField]
    BossAI bossBehaviour;

    [SerializeField]
    PlayerController player;

    [SerializeField]
    CameraManager camera;

    [SerializeField]
    GameObject breakable;

    [SerializeField]
    GameObject minecart; 

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartCutscene()); 
    }

    IEnumerator StartCutscene()
    {
        yield return new WaitForSeconds(4);

        camera.enabled = true;
        camera.GetComponent<Animator>().enabled = false; 
        player.enabled = true;
        bossBehaviour.enabled = true;
        breakable.SetActive(false); 
    }

    public void EndLevel()
    {
        player.enabled = false;
        player.transform.rotation = Quaternion.Euler(0, -90, 0);

        StartCoroutine(EndOfLevel()); 
    }

    IEnumerator EndOfLevel()
    {

        minecart.GetComponent<Animator>().Play("MinecartCutscene");

        yield return new WaitForSeconds(0.5f);

        bossBehaviour.enabled = false;

        yield return new WaitForSeconds(0.5f);

        bossBehaviour.GetComponentInChildren<Animator>().Play("Death");

        yield return new WaitForSeconds(1.5f);

        player.transform.rotation = Quaternion.Euler(0, 90, 0);

        while (true)
        {
            yield return null; 
            player.transform.position += player.transform.forward * 6 * Time.deltaTime; 
        }
    }
}
