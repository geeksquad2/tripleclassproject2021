﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FindAllTiles : MonoBehaviour
{
    //This script spawns 3d levels based on data collected from a tilemap

    [SerializeField]
    GameObject objectToSpawn;
    [SerializeField]
    Tilemap targetMap;

    private void Start()
    {
        InitializeTiles(targetMap); 
    }

    //Gets all tiles on the tilemap and spawns desired geometry at their position
    public void InitializeTiles(Tilemap tilemap)
    {
        List<Vector3Int> gameTiles = new List<Vector3Int>();
        // Add each tile to the tile list
        for (int n = tilemap.cellBounds.xMin; n < tilemap.cellBounds.xMax; n++)
        {
            for (int p = tilemap.cellBounds.yMin; p < tilemap.cellBounds.yMax; p++)
            {
                Vector3Int localPlace = (new Vector3Int(n, p, (int)tilemap.transform.position.y));
                if (tilemap.HasTile(localPlace))
                {
                    //Tile at "place"
                    GameObject inst = Instantiate(objectToSpawn, tilemap.CellToWorld(localPlace), Quaternion.identity);
                    gameTiles.Add(localPlace);
                }
            }
        }
    }
}
