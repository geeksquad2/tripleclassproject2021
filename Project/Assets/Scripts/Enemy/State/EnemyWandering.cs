﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyWandering : MonoBehaviour
{
    private FieldOfView Fov;
    private NavMeshAgent navmeshAgent;
    private float checkRate;
    private float nextCheck;

    private Transform e_trans;
    private NavMeshHit navHit;
    private Vector3 wanderTarget;
    public int stateTirggerVaule;

    [Header("State Inspector")]
    [SerializeField]
    private float wanderRange;
    [SerializeField]
    private float currentFov;
    [SerializeField]
    bool lookingForLocation;
    [SerializeField]
    bool searchFailed;
    public bool onRoute = false;
    [SerializeField]
    float onRouteTime;



    void OnEnable()
    {
        GetComponentInChildren<Animator>().Play("Walk");
        InitialData();
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll; 
        searchFailed = false;
    }

    void Update()
    {
        stateChecker();
    }
    void InitialData()
    {
        e_trans = transform;
        Fov = GetComponent<FieldOfView>();
        navmeshAgent = GetComponent<NavMeshAgent>();
        onRoute = false;
    }

    void StateConditionCheck()
    {
        //Set condition here
        //Fov.viewAwareness <= stateTirggerVaule &&
        if (!onRoute)
        {
            if (RandomTarget(e_trans.position, wanderRange, out wanderTarget))
            {
                lookingForLocation = true;
                if (navmeshAgent.enabled != false)
                {
                    if (wanderTarget != null)
                    {
                        navmeshAgent.SetDestination(wanderTarget);
                        onRoute = true;
                    }
                }
            }
            else
            {
                lookingForLocation = false;
            }
        }
        if (onRoute)
        {
            lookingForLocation = false;
            float Distance;
            Distance = Vector3.Distance(e_trans.position, wanderTarget);
            onRouteTime = Distance / navmeshAgent.speed;
            debugging();
        }
        if (onRouteTime <= 0.00001)
        {
            onRoute = false;
        }
    }

    bool RandomTarget(Vector3 centre, float range, out Vector3 result)
    {
        Vector3 randomPoint = centre + Random.insideUnitSphere * wanderRange;
        if (NavMesh.SamplePosition(randomPoint, out navHit, 1.0f, NavMesh.AllAreas))
        {
            searchFailed = false;
            result = navHit.position;
            return true;
        }
        else
        {
            searchFailed = true;
            result = centre;
            return false;
        }

    }

    void stateChecker()
    {
        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            StateConditionCheck();

        }
        currentFov = Fov.viewAwareness;
    }

    void debugging()
    {
        Debug.DrawLine(e_trans.position, wanderTarget, Color.green);
    }

}
