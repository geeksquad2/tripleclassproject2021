﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyChasing : MonoBehaviour
{
    //This script controls the enemy when it is chasing the player

    //Vision stats
    private FieldOfView Fov;
    private NavMeshAgent navmeshAgent;
    public List<Transform> stateVisibleTargets = new List<Transform>();

    [Header("State Inspector")]
    private Vector3 oringalLocation;
    [SerializeField]
    private Vector3 enemy_TargetPosition;
    [SerializeField]
    private float stateTirggerVaule;

    private void OnEnable()
    {
        GetComponentInChildren<Animator>().Play("Chase"); 
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    void Start()
    {
        InitialData();
    }
    void Update()
    {
        
        StateConditionCheck();
    }

    //Initialize stats
    void InitialData()
    {
        Fov = GetComponent<FieldOfView>();
        navmeshAgent = GetComponent<NavMeshAgent>();
        oringalLocation = transform.position;
    }

    //Tests current state and sets target position
    void StateConditionCheck()
    {
        ///////////////////////////////////////////Condition to import into FSM///////////////////////////////////
        if (navmeshAgent.enabled != false)
        {
            navmeshAgent.SetDestination(enemy_TargetPosition);
            stateVisibleTargets = Fov.visibleTargets;
            if (Fov.viewAwareness > stateTirggerVaule)
            {
                for (int i = 0; i < stateVisibleTargets.Count; i++)
                {
                    enemy_TargetPosition = stateVisibleTargets[i].position;
                }
            }
            else
            {
                enemy_TargetPosition = oringalLocation;
            }
        }
    }


   

}
