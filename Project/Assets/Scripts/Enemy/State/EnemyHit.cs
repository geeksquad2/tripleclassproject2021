﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHit : MonoBehaviour
{
    //This script controls the enemy when the player is using a megnet on it

    public NavMeshAgent navmeshAgent;
    private EnemyWandering enemyWandering;
    public Rigidbody body;

    bool magnetic; 

    [SerializeField]
    float magnetForce;
    [SerializeField]
    float speedCap;

    float groundHeight;

    void OnEnable()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation; 
        InitialData();
        magnetic = GetComponent<EnemyBehaviourManager>().isMagnetic; 
        groundHeight = 0.5f;
    }

    void FixedUpdate()
    {
        RegulateVelocity();
    }

    //Initialize variables
    void InitialData()
    {
        navmeshAgent = GetComponent<NavMeshAgent>();
        body.useGravity = true;
        body = GetComponent<Rigidbody>();
        enemyWandering = GetComponent<EnemyWandering>();
        enemyWandering.onRoute = false;
    }
    
    //Move the enemy in magnet direction
    public void AddMagneticForce(Vector3 direction)
    {
        if (magnetic)
        {
            body.constraints = RigidbodyConstraints.FreezeRotation;
            direction = direction.normalized;

            body.position = new Vector3(body.position.x, groundHeight, body.position.z);
            body.useGravity = false;
            body.velocity = direction * magnetForce;
        }
    }
    //Let go of the enemy
    public void LetGo()
    {
        body.useGravity = true;

    }
    //Cap velocity at set speed
    void RegulateVelocity()
    {
        if (body.velocity.magnitude > speedCap)
        {
            body.velocity = body.velocity.normalized * speedCap;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 14)
        {
            groundHeight = collision.transform.position.y + 0.5f;
        }
    }
}
