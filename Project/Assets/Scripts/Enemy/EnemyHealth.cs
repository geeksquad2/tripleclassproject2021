﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    //This script controls enemy health and damage

    //Speed an object has to be moving to damage the enemy
    public float speedToDamage; 
    public int hp; 

    //Take specified amount of damage and destroy object if health is zero
    public void TakeDamage(int amount)
    {
        hp -= amount; 

        if(hp <= 0)
        {
            StartCoroutine(DeathAnim()); 
        }
    }

    //On collision with a moveable object if it is going fast enough take damage
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Movable")
        {
            if (collision.gameObject.GetComponent<MoveFromMagnet>().moving)
            {
                TakeDamage(1);
            }
        }
        else if (collision.gameObject.tag == "Kart")
        {
            if (collision.gameObject.GetComponent<MineCart>().moving)
            {
                TakeDamage(1);
            }
        } else if (collision.gameObject.tag == "Lava")
        {
            TakeDamage(1); 
        }
    }
    IEnumerator DeathAnim()
    {
        GetComponentInChildren<Animator>().Play("Death");
        GetComponent<EnemyBehaviourManager>().enabled = false;
        GetComponent<EnemyWandering>().enabled = false;
        GetComponent<EnemyChasing>().enabled = false; 
        transform.GetChild(0).gameObject.SetActive(false); 
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
