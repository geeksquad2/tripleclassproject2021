﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyBehaviourManager : MonoBehaviour
{
    //This script manages the enemy's behaviour system

    //Stats for enemy state
    public bool isEnemyFalling;
    private FieldOfView Fov;
    private NavMeshAgent navmeshAgent;
    [SerializeField]
    private float triggerValue;

    public bool isMagnetic; 

    //Component information
    [System.Serializable]
    public class Scripts
    {
        public MonoBehaviour mainScript;
        public bool isEnable;
        //public Component attachedTo; 
    }
    [SerializeField]
    private MonoBehaviour[] components;
    [SerializeField]
    private List<Scripts> scripts = new List<Scripts>();

    void Start()
    {
        container();
        InitialData();

    }
    void Update()
    {
        ScriptEnable();
        StateSwitch();
        groundChecker();
    }
    //Get components for states
    void AddScript(MonoBehaviour c, bool isEnabled)
    {

        Scripts sp = new Scripts();
        foreach (Scripts script in scripts)
        {
            sp.mainScript = c;
            sp.isEnable = isEnabled;
        }

        for (int i = 0; i < scripts.Count; i++)
        {
            if (scripts[i].mainScript == null)
            {
                scripts[i].mainScript = components[i];
                scripts[i].isEnable = components[i].enabled;
            }
        }
        scripts.Add(sp);
    }
    //Enable the first state
    void ScriptEnable()
    {
        for (int i = 0; i < scripts.Count; i++)
        {
            components[i].enabled = scripts[i].isEnable;
        }
    }
    //Container of all monobehaviour states on object
    void container()
    {
        bool enabled;
        MonoBehaviour c;
        foreach (MonoBehaviour mono in components)
        {
            c = mono;
            enabled = mono.enabled;
            AddScript(c, enabled);
        }

    }
    //When hit with magnet enable the magnet movement script
    public void GetHit()
    {
        if (isMagnetic)
        {
            scripts[3].isEnable = true;
            navmeshAgent.enabled = false;
        }
    }
    //If the enemy is not falling enable the movement again
    public void GetBacktoNormal()
    {
        if (isMagnetic)
        {
            if (!isEnemyFalling)
            {
                scripts[3].isEnable = false;
                navmeshAgent.enabled = true;
            }
        }
    }
    //Switch current state
    public void StateSwitch()
    {
        if (scripts[3].isEnable==false)
        {
            if(Fov.viewAwareness <= triggerValue)
            {
                scripts[4].isEnable = true;
                scripts[5].isEnable = false;
            }
            else
            {
                scripts[4].isEnable = false;
                scripts[5].isEnable = true;
            }
        }
        else
        {
            scripts[4].isEnable = false;
            scripts[5].isEnable = false;
        }
    }
    //Initialize navmesh and vision data
    void InitialData()
    {
        Fov = GetComponent<FieldOfView>();
        navmeshAgent = GetComponent<NavMeshAgent>();
    }
    //Check if the enemy is on the ground or over lava
    void groundChecker()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position,-Vector3.up, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, -Vector3.up, Color.white); 

            if (hit.collider.gameObject.layer == 9)
            {
                isEnemyFalling = true;
            }
            else
            {
                isEnemyFalling = false;
            }
        }


    }
}
