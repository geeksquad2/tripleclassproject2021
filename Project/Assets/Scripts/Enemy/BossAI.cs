﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAI : MonoBehaviour
{
    //This script controls the boss behaviour
 
    //List of boss damage colliders
    public List<Collider> damageColliders = new List<Collider>();

    [SerializeField]
    MeshRenderer indicator; 

    //States
    public enum State { Following, Accelerating, ChargingFire, BreathingFire, ChargingBite, Biting }
    public State currentState;

    [SerializeField]
    float speed;

    [SerializeField]
    float chaseDistance;

    [SerializeField]
    Transform player;

    float attackTimer; 
    float animationTimer;
    float stompTimer;

    Animator anim;

    [SerializeField]
    GameObject fireParticles;

    bool endgame;

    Rigidbody body;

    //Audio

    AudioSource audioSource;

    public AudioClip roar;
    public AudioClip stomp;
    public AudioClip fire;
    public AudioClip attack;
    public AudioClip explosion;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        Invoke("ExplosionPlay", 1.2f);
    }

    void ExplosionPlay()
    {
        audioSource.PlayOneShot(explosion, 0.7f);
    }

    private void OnEnable()
    {
        //Set current state and disable damage colliders
        currentState = State.Following;
        anim = GetComponentInChildren<Animator>();

        body = GetComponent<Rigidbody>();
        body.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
        endgame = false; 

        attackTimer = 0;

        for (int i = 0; i < damageColliders.Count; i++)
        {
            damageColliders[i].enabled = false; 
        }
    }
    private void OnDisable()
    {
        body.velocity = Vector3.zero; 
    }

    private void FixedUpdate()
    {
        //Perform action based on current state
        if(currentState == State.Following)
        {
            Follow();
        } 
        else if (currentState == State.Biting)
        {
            Biting(); 
        } 
        else if (currentState == State.BreathingFire)
        {
            BreathingFire(); 
        }
    }

    //The boss moves towards the player at a set speed. If too far speed up. If fire timer is full switch to fire state
    void Follow()
    {

        float multiplier = (player.position.x - transform.position.x) - chaseDistance;
        if (multiplier < 1)
        {
            multiplier = 1;
        }

        body.velocity = transform.forward * speed * multiplier; 
        attackTimer += Time.deltaTime;
        stompTimer += Time.deltaTime;

        if (transform.position.z < player.position.z || transform.position.z > player.position.z)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, player.position.z), speed * Time.deltaTime);
        }

        if (attackTimer > 7.5f && !endgame)
        {
            currentState = State.ChargingFire;
            ChargingFire();
        }

        if (stompTimer > 1)
        {
            audioSource.PlayOneShot(stomp, 0.5f);
            stompTimer = 0;
        }
    }


    //Start the bite animation, reset the animation timer, and start the bite state
    void ChargingBite()
    {
        anim.Play("Bite");
        body.velocity = Vector3.zero; 
        animationTimer = 0f;
        currentState = State.Biting;
        audioSource.PlayOneShot(attack, 1.0f);
    }

    //Bite attack
    void Biting()
    {
        animationTimer += Time.deltaTime;

        //While the boss is in the air move forward and enable colliders
        if (animationTimer > 1f && animationTimer < 1.75f)
        {
            transform.position += transform.forward * speed * 2 * Time.deltaTime;
            attackTimer = 0f; 

            for (int i = 0; i < damageColliders.Count; i++)
            {
                damageColliders[i].enabled = true;
            }
        } else if (animationTimer > 1.75f)
        {
            transform.position -= transform.forward * speed * 2 * Time.deltaTime;
        }
        
        //Once the animation is complete reset to the default state
        if(animationTimer > 2.5f)
        {
            anim.Play("Walk");
            currentState = State.Following;
            for (int i = 0; i < damageColliders.Count; i++)
            {
                damageColliders[i].enabled = false;
            }
        }
    }

    //Start the fire animation and reset the animation timer 
    void ChargingFire()
    {
        body.velocity = Vector3.zero;
        anim.Play("Fire");

        animationTimer = 0f;
        currentState = State.BreathingFire;

        audioSource.PlayOneShot(roar, 0.5f);
    }

    void BreathingFire()
    {
        //Foreshadow attack location
        if(animationTimer <= 0.5f)
        {
            indicator.material.color = new Color(1, 0, 0, animationTimer*2);
        }
        else
        {
            indicator.material.color = new Color(1, 0, 0, 2f - animationTimer*2);
        }
        
        animationTimer += Time.deltaTime;

        //Once the animation is at a specific time start the fire particles
        if(animationTimer > 1f)
        {
            fireParticles.SetActive(true);
;       }

        //Once the attack is complete return to default state 
        if (animationTimer > 3.5f)
        {
            attackTimer = 0f;
            anim.Play("Walk");
            currentState = State.Following;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //If following the player and the boss gets close switch to the bite attack 
        if (other.gameObject.tag == "Player" && currentState == State.Following && !endgame)
        {
            currentState = State.ChargingBite;
            ChargingBite();
        }
        else if (other.gameObject.tag == "End")
        {
            endgame = true; 
        }
    }
}
