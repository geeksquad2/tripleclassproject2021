﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    //This script tracks how long the fireball is alive then disables itself after 5 seconds
    float timeAlive;

    private void OnEnable()
    {
        //timeAlive = 0f;
        Invoke("DisableObject", 5f);
    }
    
    void DisableObject()
    {
        gameObject.SetActive(false);
    }
}
