﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.Analytics;

public class SceneLoader : MonoBehaviour
{
    //This script loads scenes from menus

    [SerializeField]
    int nextLvl;

    int scene;

    private Dictionary<string, object> levelParameters = new Dictionary<string, object>();
    private Dictionary<string, object> sessionParameters = new Dictionary<string, object>();
    private Dictionary<string, object> bossParameters = new Dictionary<string, object>();

    private void Start()
    {
        scene = SceneManager.GetActiveScene().buildIndex;
        levelParameters.Add("gems", 0);
        levelParameters.Add("deaths", 0);
        levelParameters.Add("time", 0.0f);

        sessionParameters.Add("gems", 0);

        bossParameters.Add("boss", 0);
    }

    //Loads a desired scene
    public void LoadScene(int number)
    {
        SceneManager.LoadScene(number); 
    }

    //Loads the next level when player touches this object
    private void OnCollisionEnter(Collision collision)
    {
        //On level complete!
        if(collision.gameObject.tag == "Player")
        {
            levelParameters["gems"] = CollectibleManager.Instance.currentCount;
            levelParameters["time"] = Time.time - collision.gameObject.GetComponent<PlayerHealth>().timer;
            levelParameters["deaths"] = PlayerPrefs.GetInt("deaths_" + scene, 0);

            AnalyticsResult result = AnalyticsEvent.Custom("level_"+scene, levelParameters);
            if (result == AnalyticsResult.Ok)
            {
                Debug.Log("LEVEL RESULTS SAVED SUCCESSFULLY AT " + Time.time);
                Debug.Log("GEMS: " + levelParameters["gems"]);
                Debug.Log("TIME: " + levelParameters["time"]);
                Debug.Log("DEATHS: " + levelParameters["deaths"]);
            }
            else
            {
                Debug.Log("LEVEL RESULTS DID NOT SAVE SUCCESSFULLY AT " + Time.time);
            }
            
            PlayerPrefs.SetInt("deaths_" + scene, 0);

            //If the next level to be loaded is the boss level, send "reached boss" analytic
            if (nextLvl == 15)
            {
                //send reached boss analytic
                bossParameters["boss"] = 1;
                AnalyticsResult altResult = AnalyticsEvent.Custom("bossReached", bossParameters);


                if (altResult == AnalyticsResult.Ok)
                {
                    Debug.Log("BOSS RESULTS SAVED SUCCESSFULLY AT " + Time.time);
                }
                else
                {
                    Debug.Log("BOSS RESULTS DID NOT SAVE SUCCESSFULLY AT " + Time.time);
                }
            }
            SceneManager.LoadScene(nextLvl);
        }
    }

    //Closes the game
    public void Quit()
    { 
        Application.Quit(); 
    }

    private void OnApplicationQuit()
    {
        sessionParameters["gems"] = CollectibleManager.Instance.totalCount;
        AnalyticsResult result = AnalyticsEvent.Custom("total_gems", sessionParameters);
        if (result == AnalyticsResult.Ok)
        {
            Debug.Log("SESSION RESULTS SAVED SUCCESSFULLY AT " + Time.time);
            Debug.Log("TOTAL GEMS: " + sessionParameters["gems"]);
        }
        else
        {
            Debug.Log("SESSION RESULTS DID NOT SAVE SUCCESSFULLY AT " + Time.time);
        }
    }
}
