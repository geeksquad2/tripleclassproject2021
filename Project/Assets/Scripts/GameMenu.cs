﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    [SerializeField]
    List<GameObject> levelButtons = new List<GameObject>();

    private void Start()
    {
        PlayerPrefs.SetInt("LV" + SceneManager.GetActiveScene().buildIndex, 1);

        if (PlayerPrefs.GetInt("LV1") == 1)
        {
            levelButtons[0].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV2") == 1)
        {
            levelButtons[1].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV3") == 1)
        {
            levelButtons[2].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV4") == 1)
        {
            levelButtons[3].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV5") == 1)
        {
            levelButtons[4].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV6") == 1)
        {
            levelButtons[5].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV7") == 1)
        {
            levelButtons[6].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV8") == 1)
        {
            levelButtons[7].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV9") == 1)
        {
            levelButtons[8].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV10") == 1)
        {
            levelButtons[9].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV11") == 1)
        {
            levelButtons[10].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV12") == 1)
        {
            levelButtons[11].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV13") == 1)
        {
            levelButtons[12].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV14") == 1)
        {
            levelButtons[13].SetActive(true);
        }
        if (PlayerPrefs.GetInt("LV15") == 1)
        {
            levelButtons[14].SetActive(true);
        }
    }
}
