﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    //This script controls the enemy's vision

    //Variables to do with objects the enemy will target
    [Header("Target Setting")]
    public LayerMask targetLayer;
    public LayerMask obstacleLayer;
    public List<Transform> visibleTargets = new List<Transform>();

    //Variables to do with enemy vision
    [Header("VisionCon Setting")]
    public float FOVRadius;
    [Range(0, 360)]
    public float FOVAngle;

    //Variables to do with enemy navmesh and navigation
    [Header("Mesh Setting")]
    public float Resolution;
    public int edgeResolveIterations;
    public float edgeDstThreshold;
    public MeshFilter viewMeshFilter;
    Mesh viewMesh;
    [Space]
    [Header("Detection(Could be use as output)")]
    public bool Detection;
    public float viewAwareness;
    [SerializeField]
    float increaseSpeed;
    [SerializeField]
    float decreaseSpeed;
    float distAdjust;


    void Start()
    {
        Detection = false;
        viewAwareness = 0;

        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        StartCoroutine("FindTargetsWithDelay", .2f);
    }

    void Update()
    {
        // If no object is spotted decrease the enemy awareness of a nearby object
        if (Detection == false)
        {
            viewAwareness -= Time.deltaTime * decreaseSpeed;
            if (viewAwareness < 0) { viewAwareness = 0; }
        }

    }

    //Wait a delay and seach for target
    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    //Draw the enemy's vision
    void LateUpdate()
    {
        DrawFieldOfView();
    }

    //Find target object to chase
    void FindVisibleTargets()
    {
        //Clear current visibility
        visibleTargets.Clear();
        Detection = false;
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, FOVRadius, targetLayer);

        //Checks all targets in vision to select the ideal one
        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < FOVAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleLayer))
                {
                    visibleTargets.Add(target);
                    Detection = true;
                    float dist = Vector3.Distance(target.transform.position, transform.position);
                    if (Detection)
                    {
                        distAdjust = FOVRadius - dist;
                        if (distAdjust < 1) { distAdjust = 1; }
                        if (dist / 2 >= FOVRadius / 2) { viewAwareness += distAdjust / FOVRadius * increaseSpeed; }
                        else if (dist / 2 < FOVRadius / 2 && dist / 2 > FOVRadius / 3) { viewAwareness += distAdjust / FOVRadius * increaseSpeed * 2; }
                        else { viewAwareness = 100; }


                        if (viewAwareness >= 100)
                        {
                            viewAwareness = 100;
                        }
                    }

                }
            }
        }

    }

    //Draws the enemy vision in the game world for the player to see
    void DrawFieldOfView()
    {
        int step = Mathf.RoundToInt(FOVAngle * Resolution);
        float stepAngleSize = FOVAngle / step;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= step; i++)
        {
            float angle = transform.eulerAngles.y - FOVAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }

            }


            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    //Finds the edge of the enemy vision
    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    //Gets information about enemy vision based on vision cone
    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, dir, out hit, FOVRadius, obstacleLayer))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * FOVRadius, FOVRadius, globalAngle);
        }
    }

    //Gets direction to an angle from current angle
    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    //Gets the information from the enemy vision
    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }

}
